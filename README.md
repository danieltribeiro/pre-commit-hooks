# pre-commit-hooks

Some out-of-the-box hooks for pre-commit.

See also:  [https://github.com/pre-commit/pre-commit](https://github.com/pre-commit/pre-commit)

### Using pre-commit-hooks with pre-commit

Add this to your  `.pre-commit-config.yaml`

```
-   repo: https://bitbucket.org/danieltribeiro/pre-commit-hooks
    rev: '0.1.2'  # Use the ref you want to point at (branch, tag or commit)
    hooks:
    -   id: mvn-install

```

### Hooks available

#### `mvn-install`

Prevent broken builds to be commited running mvn clean install before commit.
